provider "aws" {
    region = "us-east-1"
}
#create vpc
resource "aws_vpc" "my_vpc" {
cidr_block = var.var_cidr
enable_dns_hostnames = true
tags= {
    name = "DEMO VPC-SHWETHA"
}
}
# list of availability zones
data "aws_availability_zones" "all" {}

# create a public subnet
resource "aws_subnet" "public_us_east_1" {
    vpc_id = aws_vpc.my_vpc.vpc_id
cidr_block = var.subnet1_cidr
availability_zone = data.aws_availability_zones.all.names[0]
tags = {
    name = "public subnet"
}
}
# create an IGW for your new vpc
resource"aws_internet_gateway" "my_vpc_IGW" {
    vpc_id = aws_vpc.my_vpc.vpc_id
    tags ={
        name = "IGW DEMO"
    }
}

#create an route table
resource"aws_route_table" "my_vpc_public" {
    vpc_id = aws_vpc.my_vpc.vpc_id
route {

cidr_block = "0.0.0.0/0"
gateway_id = aws_internet_gateway.my_vpc_IGW.vpc_id
}
tags = {
    name = "DEMO PUBLIC ROUTE TABLE"
}
}
# associate route table to subnet
resource "aws_route_table_association" "my_vpc_us_east_1_public" {
    subnet_id = aws_subnet.public_us-east-1.vpc_id
    route_table_id = aws_route_table.my_vpc_public.vpc_id
}

#create securiry group
resource "aws_security_group" "instance" {
    name = "BHAVIN-example-instance"
    vpc_id = aws_vpc.my_vpc.id 
}
# Allow all outbound
egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}
# Allow for SSH
ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [0.0.0.0/0]
}

# create EC2 server
resource"aws_instance" "server" {
    ami = var.amiid
    instance_type = var.instance_type
    key_name = var.pemfile
    vpc_security_group_ids = [aws_security_group.instance.id]
subnet_id = aws_subnet.public_us_east_1.id
availability_zone = data.aws_availability_zones.all.names[0]
associate_public_ip_address = true
tags = {
    name = "dbserver"
}
}

# private subnet
resource "aws_subnet" "private_us_east_1" {
    vpc_id = aws_vpc.my_vpc.vpc_id
cidr_block = var.subnet2_cidr
availability_zone = data.aws_availability_zones.all.names[0]
tags = {
    name = "private subnet"
}
}
# create a route table for your DB

resource "aws_route_table" "my_vpc_private"
{
    vpc_id = aws_vpc.my_vpc.vpc_id
route
{
cidr_block = "0.0.0.0/0"
gateway_id = aws_internet_gateway.my_vpc_IGW.vpc_id
}
tags = {
    name = "DEMO PRIVATE ROUTE TABLE"
}
}
#associate the DB RT to subnet
resource "aws_route_table_association" "my_vpc_us_east_1_private" {
    subnet_id = aws_subnet.private_us-east-1.vpc_id
    route_table_id = aws_route_table.my_vpc_private.vpc_id
}

#create a SG that applied to DB
resource "aws_security_group" "instance" {
    name = "BHAVIN-example-instance"
    vpc_id = aws_vpc.my_vpc.id 
}
# Allow all outbound
egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}
# Allow for SSH
ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = [0.0.0.0/0]
}
# create EC2 SERVER
resource"aws_instance" "server" {
    ami = var.amiid
    instance_type = var.instance_type
    key_name = var.pemfile
    vpc_security_group_ids = [aws_security_group.instance.id]
subnet_id = aws_subnet.private_us_east_1.id
availability_zone = data.aws_availability_zones.all.names[0]

associate_public_ip_address = true
tags = {
    name = "dbserver"
}
}
